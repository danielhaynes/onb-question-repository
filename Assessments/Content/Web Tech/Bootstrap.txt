==========
Objective: Apply the grid model to create a specific layout.
==========

[[
You are building a web UI that is primarily used on modern laptops.
The UI has three, equally spaced column.  When the UI is viewed on
a tablet these three column should stack on top of each other.

Which combination of Bootstrap classes satisfies this scenario?
]]

[[ SOLUTION
<div>
  <div class='col-lg-4 col-md-4 col-sm-4'>LEFT</div>
  <div class='col-lg-4 col-md-4 col-sm-4'>CENTER</div>
  <div class='col-lg-4 col-md-4 col-sm-4'>RIGHT</div>
</div>
]]

1: <pre>&LT;div&GT;
  &LT;div class='col-lg-4 col-md-2 col-sm-1 col-sm-offset-0'&GT;LEFT&LT;/div&GT;
  &LT;div class='col-lg-4 col-md-2 col-sm-1 col-sm-offset-1'&GT;CENTER&LT;/div&GT;
  &LT;div class='col-lg-4 col-md-2 col-sm-1 col-sm-offset-2'&GT;RIGHT&LT;/div&GT;
&LT;/div&GT;</pre>
2: <pre>&LT;div&GT;
  &LT;div class='col-lg-4 col-sm-stack col-sm-offset-0'&GT;LEFT&LT;/div&GT;
  &LT;div class='col-lg-4 col-sm-stack col-sm-offset-1'&GT;CENTER&LT;/div&GT;
  &LT;div class='col-lg-4 col-sm-stack col-sm-offset-2'&GT;RIGHT&LT;/div&GT;
&LT;/div&GT;</pre>
*3: <pre>&LT;div&GT;
  &LT;div class='col-lg-4 col-md-4 col-sm-4'&GT;LEFT&LT;/div&GT;
  &LT;div class='col-lg-4 col-md-4 col-sm-4'&GT;CENTER&LT;/div&GT;
  &LT;div class='col-lg-4 col-md-4 col-sm-4'&GT;RIGHT&LT;/div&GT;
&LT;/div&GT;</pre>
4: <pre>&LT;div&GT;
  &LT;div class='col-lg-4 col-sm-stack'&GT;LEFT&LT;/div&GT;
  &LT;div class='col-lg-4 col-sm-stack'&GT;CENTER&LT;/div&GT;
  &LT;div class='col-lg-4 col-sm-stack'&GT;RIGHT&LT;/div&GT;
&LT;/div&GT;</pre>
