==========
Objective: Write the doctype element of an HTML5 page.
==========

[[
Which declaration begins a standard HTML5 page?
]]
*1: <code>&LT;!DOCTYPE html&GT;</code>
2: <code>&LT;!doctype html5&GT;</code>
3: <code>&LT;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0//EN" "http://www.w3.org/TR/html5/strict.dtd"&GT;</code>
4: No <code>doctype</code> declaration is needed for a standard HTML5 page.


==========
Objective: Create elements to include CSS and JavaScript.
==========

[[
Which HTML5 tag adds a CSS script to an HTML5 page for use in printing?
]]
1: <code>&LT;link type="print" href="file.css"&GT;/code>
2: <code>&LT;link media="print" href="file.css"&GT;</code>
3: <code>&LT;link type="text/css" href="file.css"&GT;</code>
4: <code>&LT;link rel="stylesheet" type="print" href="file.css"&GT;</code>
*5: <code>&LT;link rel="stylesheet" media="print" href="file.css"&GT;/code>
6: <code>&LT;link rel="stylesheet" type="text/css" href="file.css"&GT;</code>


==========
Objective: Write a <script> tag that satisfies a loading scenario.
==========

[[
You have a script file that must execute after the complete HTML page has finished parsing.

Which HTML5 tag accomplishes this goal?
]]
1: <code>&LT;script src="script.js" /&GT;</code>
2 <code>&LT;script src="script.js" defer /&GT;</code>
3 <code>&LT;script src="script.js" async="true" /&GT;</code>
4 <code>&LT;script src="script.js"&GT;&LT;script/&GT;</code>
*5: <code>&LT;script src="script.js" defer&GT;&LT;script/&GT;</code>
6: <code>&LT;script src="script.js" async="true"&GT;&LT;script/&GT;</code>

[[
What does the "defer" attribute mean in a <code>&LT;script&GT;</code> tag?
]]
1. Defers execution until the user approves the code
*2. Defers execution until the page is finished loading
3. Defers execution until the script is finished loading
4. Defers execution until all other deferred scripts have been loaded
