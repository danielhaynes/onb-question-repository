(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): John Ghidiu
(Course Site):
(Course Name):
(Course URL):
(Discipline): JavaScript
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What are the variable types in JavaScript? (Select all that apply)
(A): String
(B): Number
(C): Boolean
(D): Array
(E): Object
(Correct): A,B,C,D,E
(Points): 1
(CF): The valid variable types in JavaScript are String, Number, Boolean, Array and Object
(WF): The valid variable types in JavaScript are String, Number, Boolean, Array and Object
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): JavasScript
(Grade style): 0
(Random answers): 1
(Question): What does a try/catch block do?
(A): Catches errors generated at run time
(B): Catches errors generated at load time
(C): Catches errors generated at parse time
(D): Catches errors generated at write time
(E): 
(Correct): A
(Points): 1
(CF): The try/catch blocks catch errors generated when code executes
(WF): The try/catch blocks catch errors generated when code executes
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): A break statement is required in a switch block.
(A): True
(B): False
(C): 
(D): 
(E): 
(Correct): B 
(Points): 1
(CF): A break statement is ideal, but not necessary, in a switch block.
(WF): A break statement is ideal, but not necessary, in a switch block.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): null is the same as undefined.
(A): True
(B): False
(C): 
(D): 
(E): 
(Correct): B 
(Points): 1
(CF): null and undefined are similar, but different values.
(WF): null and undefined are similar, but different values.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What does the "delete" operator do?
(A): Deletes the value indexed in an array
(B): Deletes a property on an object
(C): Deletes the most recently printed character on the console
(D): Deletes the most recently declared variable
(E): 
(Correct): B
(Points): 1
(CF): The delete operator is used to delete a property from an object.
(WF): The delete operator is used to delete a property from an object.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Which of the following would access the third value in the array 'someArray'?
(A): someArray.2
(B): someArray.3
(C): someArray[2]
(D): someArray[3]
(E): third(someArray)
(Correct): C
(Points): 1
(CF): Arrays are 0 based, and the square brackets are used to access values within an array.
(WF): Arrays are 0 based, and the square brackets are used to access values within an array.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What does a function return when no explicit return statement exists?
(A): null
(B): undefined
(C): void
(D): 0
(E): The last value
(Correct): B 
(Points): 1
(CF): Functions which have no explicit return statement return the undefined value.
(WF): Functions which have no explicit return statement return the undefined value.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What is the result of dividing by 0?
(A): null
(B): An error is thrown
(C): NaN
(D): Infinity
(E): 
(Correct): D
(Points): 1
(CF): Divding by 0 will return the value Infinity.
(WF): Divding by 0 will return the value Infinity. 
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What delimiters can you define a string with? (Select all that apply)
(A): Single quotes (')
(B): Double quotes (")
(C): Back ticks (`)
(D): Tildes (~)
(E): 
(Correct): A,B 
(Points): 1
(CF): Strings are defined with either single or double quotes.
(WF): Strings are defined with either single or double quotes. 
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): JavaScript explicitly permits threading.
(A): True
(B): False
(C): 
(D): 
(E): 
(Correct): B 
(Points): 1
(CF): JavaScript does not explicitly permit threading.
(WF): JavaScript does not explicitly permit threading.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): How is JavaScript typed? (Select all that apply)
(A): Statically
(B): Dynamically
(C): Variable
(D): Constantly
(E): 
(Correct): B
(Points): 1
(CF): JavaScript is a dynamically, not statically, typed language.
(WF): JavaScript is a dynamically, not statically, typed language.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Is the == operator the same as the === operator? For example, is the expression [2 == 2] the same as [2 === "2"]?
(A): Yes, but only for primitives
(B): No, never
(C): Yes, but only for objects
(D): Yes, but only for null and undefined
(E): 
(Correct): B
(Points): 1
(CF): The == and === operators are similar, but one checks for type and the other does not.
(WF): The == and === operators are similar, but one checks for type and the other does not.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What happens when the "new" keyword is not used when instantiating an object?
(A): The object is not created
(B): The object context is set to the global namespace
(C): The object uses the most recently created context as its own
(D): The code will throw an error
(E): 
(Correct): B
(Points): 1
(CF): The new keyword creates a new, empty context for the newly created object.
(WF): The new keyword creates a new, empty context for the newly created object.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): truefalse
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Properties can be added to a function.
(A): True
(B): False
(C): 
(D): 
(E): 
(Correct): A 
(Points): 1
(CF): Functions are objects and can therefore have properties assigned.
(WF): Functions are objects and can therefore have properties assigned.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Which of these are valid values for a throw statement?
(A): null
(B): undefined
(C): {}
(D): {message: 'Cannot divide by 0'}
(E): 
(Correct): A,B,C,D
(Points): 1
(CF): Nearly any value can be thrown.
(WF): Nearly any value can be thrown.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What does "use strict" mean?
(A): Print stern warning messages.
(B): Opt in to a restricted variant of JavaScript
(C): Require semi-colons at the end of each statement
(D): Require single quotes for string literals
(E): "use strict" is meaningless in JavaScript
(Correct): B
(Points): 1
(CF): Strict mode enforces certain behaviours in order to help provide clearer, more deterministic code across runtimes.
(WF): Strict mode enforces certain behaviours in order to help provide clearer, more deterministic code across runtimes.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): truefalse
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): For loops provide scoping.
(A): True
(B): False
(C): 
(D): 
(E): 
(Correct): B 
(Points): 1
(CF): For loops do not provide scoping; scoping is most commonly seen within functions.
(WF): For loops do not provide scoping; scoping is most commonly seen within functions.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Can you define a function within a function?
(A): Yes, but it is hoisted to the global namespace
(B): Yes, and it remains scoped to the enclosing function
(C): No, an error is thrown at runtime
(D): No, it will silently fail
(E): 
(Correct): B
(Points): 1
(CF): Functions can be defined nearly anywhere, and can be embedded within functions.
(WF): Functions can be defined nearly anywhere, and can be embedded within functions.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate 
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Does a native promise API exist within the JavaScript specification?
(A): Yes, and it is used heavily by jQuery
(B): Yes, and it can be used all the time
(C): No
(D): Yes, but it is only available in strict mode
(E): 
(Correct): C` 
(Points): 1
(CF): The JavaScript specification does not provide a native promise API.
(WF): The JavaScript specification does not provide a native promise API.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate 
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Can a function be defined as a variable?
(A): Yes, but only when not using strict mode
(B): No
(C): Yes, but only when using strict mode
(D): Yes, but only within a closure
(E): Yes, all the time
(Correct): E
(Points): 1
(CF): A function can be defined as a variable, as in: var someFunction = function() {};.
(WF): A function can be defined as a variable, as in: var someFunction = function() {};.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate 
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What is the maximum length of an array?
(A): (2^32)-1
(B): Determined by the browser or OS
(C): 4,294,967,295
(D): The number of indexes provided by an unsigned, 32 bit integer
(E): 
(Correct): A,C,D
(Points): 1
(CF): A,C and D are all represent the same value, and this is the maximum length of an array.
(WF): A,C and D are all represent the same value, and this is the maximum length of an array.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate 
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): When is the default keyword used? (Select all that apply)
(A): As a catch-all for a switch block
(B): When instantiating a variable with an unknown value
(C): To specify default values for function parameters
(D): To provide a default value for deleted properties
(E): 
(Correct): A
(Points): 1
(CF): The default keyword exists as a catch-all for a switch block.
(WF): The default keyword exists as a catch-all for a switch block.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate 
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What does this output: var someArray = [1,2,5]; console.log(someArray[3]);?
(A): 3
(B): 5
(C): undefined
(D): null
(E): An error is thrown
(Correct): C
(Points): 1
(CF): Attempts to access an array index which does not exist results in the undefined value being returned.
(WF): Attempts to access an array index which does not exist results in the undefined value being returned.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Intermediate 
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Are variables hoisted in a function?
(A): Yes, but only when strict mode is enabled
(B): Yes, always
(C): Yes, but only when strict mode is not enabled
(D): No, never
(E): 
(Correct): B
(Points): 1
(CF): Variables are always hoisted within a function.
(WF): Variables are always hoisted within a function.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Can you tell if a constructor is called with the new keyword?
(A): Yes, by checking "this instanceof window"
(B): Yes, by checking "this typeof Object"
(C): Yes, but only in strict mode
(D): No, unless strict mode is not enabled
(E): Yes, by checking "this === window"
(Correct): E
(Points): 1
(CF): If a function constructor is not invoked with the new keyword, the this context is window.
(WF): If a function constructor is not invoked with the new keyword, the this context is window.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): When is a semi-colon required? (Select all that apply)
(A): At the end of each line, when using strict mode
(B): At the end of each statement, when using strict mode
(C): After a for loop, if block or function declaration
(D): None of the above
(E): 
(Correct): D
(Points): 1
(CF): Semi colons are rarely required, though they should be used at the end of each statement.
(WF): Semi colons are rarely required, though they should be used at the end of each statement.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): truefalse
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): JavaScript provides prototypical inheritance.
(A): True
(B): False
(C): 
(D): 
(E): 
(Correct): A 
(Points): 1
(CF): Inheritance is prototypical in JavaScript.
(WF): Inheritance is prototypical in JavaScript.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What does the "defer" attribute mean in a <script> tag?
(A): Defers execution until the user approves the code
(B): Defers execution until the script is finished loading
(C): Defers execution until the page is finished loading
(D): Defers execution until all other deferred scripts have been loaded
(E): 
(Correct): C
(Points): 1
(CF): The "defer" attribute instructs the browser to defer execution until the document has fully loaded.
(WF): The "defer" attribute instructs the browser to defer execution until the document has fully loaded.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): What does this code output: var someValue; console.log(!!someValue);
(A): true
(B): false
(C): This code throws an error
(D): Not enough information to determine the output
(E): 
(Correct): A
(Points): 1
(CF): The output is true because !s is false (because the string value is truthy) and the negation (provided by the second ! operator) of false is true.
(WF): The output is true because !s is false (because the string value is truthy) and the negation (provided by the second ! operator) of false is true.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): When would you use \s\S instead of . In a regular expression?
(A): When checking UTF-16 strings
(B): When using strict mode and looking for a match
(C): When searching for a a multiline match
(D): When searching for the string literal "sS"
(E): 
(Correct): B
(Points): 1
(CF): . does not always match multiline expressions, whereas \s\S will.
(WF): . does not always match multiline expressions, whereas \s\S will.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Is null considered an object?
(A): Only when strict mode is used
(B): Never when strict mode is used
(C): Always
(D): Never
(E): 
(Correct): C 
(Points): 1
(CF): null is considered an object in JavaScript.
(WF): null is considered an object in JavaScript.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): JavaScript
(Grade style): 0
(Random answers): 1
(Question): Is undefined considered an object?
(A): Only when strict mode is used
(B): Never when strict mode is used
(C): Always
(D): Never
(E): 
(Correct): D 
(Points): 1
(CF): undefined is not an object; rather, it is simply a value.
(WF): undefined is not an object; rather, it is simply a value.
(STARTIGNORE)
(Hint):
(Subject): Language
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)