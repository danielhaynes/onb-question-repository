(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category): 0

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses


For more help on adding questions, go to http://www.classmarker.com/a/help/#tests and look under the 'Import questions' section

(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site):Udemy
(Course Name): Consulting Skills Series Communication
(Course URL):https://www.udemy.com/consulting-skills-series-communication/?dtcode=NXFwWSz1vMzW
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 1
(Question): When you are having problems getting along with an individual what are 2 ways of dealing with them. 
(A): Cut them off and get your point across
(B): Attempt to make them an ally
(C): Contain their destructive potential
(D): Tell them they are wrong and ignore them for the rest of the consultation
(Correct): B,C
(Points): 1
(CF): There are 2 defined ways to try and deal with an adversary, either contain them or attempt to convert them to an ally.
(WF): There are 2 defined ways to try and deal with an adversary, either contain them or attempt to convert them to an ally.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When setting up a conference call it is generally best to: 
(A): Have an agenda
(B): Invite everyone in the organization to ensure all people are covered
(C): Send the invite 2 minutes before the meeting starts
(D): Have no purpose to the meeting, just have one to get some more chat time in your day
(Correct): A
(Points): 1
(CF): At the very least you should have an agenda and a purpose for a conference call.
(WF): At the very least you should have an agenda and a purpose for a conference call.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): If a reader does not respond to your email, how should you react? 
(A): Spam them with several more emails to bring it to the top of their inbox
(B): Send a follow up email asking why they have not responded
(C): Keep in mind that the individual might be busy or may have missed your email and give them a call
(D): Block them from your email and end any further email communications
(Correct): C
(Points): 1
(CF): Sometimes people either miss an email among other emails or are too busy to respond, it is best to follow up with a phone call if that appears to be the case.
(WF): Sometimes people either miss an email among other emails or are too busy to respond, it is best to follow up with a phone call if that appears to be the case.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Different people will have different expectations of you based on their role.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Different people will in fact have different expectations based on their role in the organization. 
(WF): Different people will in fact have different expectations based on their role in the organization.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What types of things can you do in a presentation to keep the audience remain alert? 
(A): Bring an air horn to blow every now and then
(B): Vary the elements within the presentation
(C): Speak in different tones, both louder and softer to connect with them
(D): Jump up and down and do cartwheels
(Correct): B,C
(Points): 1
(CF): It is good practice to vary the elements within your presentation as well as alter your speaking tone to keep people engaged.
(WF): It is good practice to vary the elements within your presentation as well as alter your speaking tone to keep people engaged.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
