(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):
(Course Site):
(Course Name):
(Course URL):
(Discipline):
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): The Date object can't perform mathematical operations on a date representation, but the ______ class can.
(A): GregorianCalendar
(B): DateBuffer
(C): CalendarBuffer
(D): DateManipulator
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 6 - Working with date values
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): The StringBuffer class is synchronized (safe to be used by multiple threads), while the StringBuilder is not.
(A): True
(B): False 
(Correct): A 
(Points): 1
(CF): Correct!
(WF): See Part 6 - Building strings with StringBuilder
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): If a variable of type int is assigned the value 4.65, what will be the value of the int variable after assignment, and why?
(A): 5, because the value is rounded up. 
(B): 4.65, because Java prevents the loss of data during assignment operations.
(C): 4, because the decimal information is truncated in order to fit the value into an int.
(D): A runtime error is thrown because this assignment cannot be done in Java.
(Correct): C
(Points): 1
(CF): Correct!
(WF): See Part 4 - Converting numeric values
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): What are the primary control loops in Java? 
(A): for-loop
(B): for-each
(C): while
(D): do-while
(E): if-then
(Correct): A,B,C,D
(Points): 1
(CF): Correct!
(WF): See Part 5 - Repeating code blocks with loops
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Files can be manipulated using FileInputStream and FileOutputStream. 
(A): True
(B): False 
(Correct): A 
(Points): 1
(CF): Correct!
(WF): See Part 11 - Managing files withe core class library
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): To retrieve data from a networkd resource, use the ___ class.
(A): URL
(B): WWW
(C): NetSource
(D): FileInputStream
(Correct): 
(Points): 1
(CF): Correct!
(WF): See Part 11 - Reading a text file from a networked resource
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is the visibility of the following method? void getInstance ()
(A): public
(B): protected
(C): private
(D): protected-private
(Correct): D
(Points): 1
(CF): Correct!
(WF): See Part 5 - Creating reusable code with methods
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Creating two methods in the same class with the same name is called...
(A): Overloading
(B): Overriding
(C): Replacing
(D): Replicating
(E): Subtyping
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 5 - Overloading method names with different signatures
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Creating a method with the same name and signature as its supertype is called...
(A): Overloading
(B): Overriding
(C): Replacing
(D): Replicating
(E): Subtyping
(Correct): B
(Points): 1
(CF): Correct!
(WF): See Part 10 - Overriding superclass methods
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): A JAR file is a .zip file and can be easily extracted and viewed by changing the file extension to .zip
(A): True
(B): False 
(Correct): A 
(Points): 1
(CF): Correct!
(WF): See Part 12 - Creating your own JAR files
(STARTIGNORE)
(Hint):
(Subject): Java
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)